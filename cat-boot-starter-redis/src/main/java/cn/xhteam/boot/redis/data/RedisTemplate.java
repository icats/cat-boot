package cn.xhteam.boot.redis.data;


import cn.xhteam.boot.core.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.Set;

@Service
public abstract class RedisTemplate extends RedisData {
    public void set(String key, String value) {
        Jedis jedis = getJedisData();
        jedis.set(key, value);
        jedis.close();
    }

    /**
     * 添加数据，带超时时间，超时自动销毁
     */
    public void setex(String key, String value, int seconds) {
        Jedis jedis = getJedisData();
        jedis.setex(key, seconds, value);
        jedis.close();
    }

    /**
     * 根据key删除数据
     */
    public void deleteByKey(String key) {
        Jedis jedis = getJedisData();
        jedis.del(key);
        jedis.close();
    }

    /**
     * 根据key查询
     */
    public String getByKey(String key) {
        Jedis jedis = getJedisData();
        String value = jedis.get(key);
        jedis.close();
        return value;
    }

    /**
     * 查询某条数据是否存在
     */
    public boolean isExist(String key) {
        Jedis jedis = getJedisData();
        boolean exist = jedis.exists(key);
        jedis.close();
        return exist;
    }

    public Set<String> getAllKey() {
        Jedis jedis = getJedisData();
        Set<String> keys = jedis.keys("*");
        return keys;
    }

    public Map<String, String> getHash(String name) {
        Jedis jedis = getJedisData();
        Map<String, String> result = jedis.hgetAll(name);
        return result;
    }
}
