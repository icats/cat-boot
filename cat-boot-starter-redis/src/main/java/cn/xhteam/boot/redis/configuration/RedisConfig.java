package cn.xhteam.boot.redis.configuration;



import cn.xhteam.boot.core.properties.PropertiesConfig;
import cn.xhteam.boot.core.stereotype.Bean;
import cn.xhteam.boot.core.stereotype.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisConfig {
    @Bean
    public JedisPool jedisPool() {
        String redisHost = PropertiesConfig.getProperty("redis.host");
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        return new JedisPool(jedisPoolConfig, redisHost,
                Integer.parseInt(PropertiesConfig.getProperty("redis.port", "6379")),
                3000, PropertiesConfig.getProperty("redis.password", ""));
    }
}
