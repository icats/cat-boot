package cn.xhteam.boot.redis.data;


import cn.xhteam.boot.core.bean.HandContainer;
import cn.xhteam.boot.core.properties.PropertiesConfig;
import cn.xhteam.boot.core.util.BeanUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
public class RedisData {
    public Jedis getJedisData() {
        Jedis jedis;
        try {
            Object obj = HandContainer.getBeanMap(BeanUtil.getClassBeanName(JedisPool.class));
            JedisPool jedisPool = (JedisPool) obj;
            jedis = jedisPool.getResource();
            jedis.select(Integer.parseInt(PropertiesConfig.getProperty("redis.database", "0")));
        } catch (Exception e) {
            throw new RuntimeException("redis连接失败，请检查连接配置是否有误...." + e.getMessage());
        }
        return jedis;
    }

}