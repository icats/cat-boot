package cn.xhteam.boot.web.configuration;

import cn.xhteam.boot.core.stereotype.Bean;
import cn.xhteam.boot.core.stereotype.Configuration;
import cn.xhteam.boot.web.interceptor.InterceptorRegistry;

@Configuration
public class WebAutoConfiguration {
    @Bean
    public InterceptorRegistry interceptorRegistry() {
        return new InterceptorRegistry();
    }
}
