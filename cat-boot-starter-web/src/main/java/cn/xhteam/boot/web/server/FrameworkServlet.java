package cn.xhteam.boot.web.server;

import cn.xhteam.boot.core.properties.PropertiesConfig;
import com.alibaba.fastjson2.util.DateUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.Date;

public abstract class FrameworkServlet extends HttpServlet {

    protected final String doHome() {
        return "<html><body><h1>欢迎使用cat-boot框架  "
                + PropertiesConfig.getProperty("cat-boot.version") + "</h1><p>它非常方便微服务开发、web服务、IOC容器、集成Redis、Mybatis-puls工具、API接口零配置开发。</p><div>" +
                DateUtils.format(new Date(), "yyyy-MM-dd HH:mm") + "</div><div></div></body></html>";
    }

    public FrameworkServlet() {
    }

    protected final void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doService(request, response);
    }

    protected abstract void doService(HttpServletRequest var1, HttpServletResponse var2) throws ServletException, IOException;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    @Override
    protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    @Override
    protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
        if (!response.containsHeader("Allow")) {
            super.doOptions(request, new HttpServletResponseWrapper(response) {
                @Override
                public void setHeader(String name, String value) {
                    if ("Allow".equals(name)) {
                        value = (value != null && !value.isEmpty() ? value + ", " : "") + "PATCH";
                    }
                    super.setHeader(name, value);
                }
            });
        }
    }
}
