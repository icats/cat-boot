package cn.xhteam.boot.web;

import cn.xhteam.boot.core.properties.PropertiesConfig;
import cn.xhteam.boot.web.core.PrintBanner;
import cn.xhteam.boot.web.load.LoadClassHand;
import cn.xhteam.boot.web.server.WebServerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;

public class CatBootApplication {
    public static final Logger logger = LoggerFactory.getLogger(CatBootApplication.class);
    protected Class<?> BootClass;

    public void start() {
        String port = PropertiesConfig.getProperty("server.port", "8080");
        WebServerManager webServerManager = new WebServerManager(Integer.parseInt(port));
        try {
            long startTime = System.currentTimeMillis();
            PropertiesConfig.loadConfig(BootClass);
            PrintBanner.loadBanner(BootClass);
            LoadClassHand.loadBean(BootClass);
            webServerManager.start();
            long endTime = System.currentTimeMillis();
            logger.info("start server sucess! \taddress：http://{}:{} \t start time:{}毫秒", InetAddress.getLocalHost().getHostAddress(), port, endTime - startTime);
        } catch (Exception e) {
            webServerManager.stop();
            throw new RuntimeException(e);
        }
    }

    public static void run(Class<?> cls, String[] args) {
        System.setProperty("java.util.logging.config.file", "");//讲自带的log打印关闭
        new CatBootApplication(cls).start();
    }

    public CatBootApplication(Class<?> cls) {
        this.BootClass = cls;
    }
}
