package cn.xhteam.boot.web.util;

import cn.xhteam.boot.core.util.Constant;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletUtils {
    public static void renderStr(HttpServletResponse response, String str) throws IOException {
        response.setContentType(Constant.CONTENT_APPLICATION_JSON + Constant.CHARSET_UTF_8);
        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type");
        response.getWriter().print(str);
    }

    public static void renderHtml(HttpServletResponse response, String str) throws IOException {
        response.setContentType(Constant.CONTENT_TEXT_HTML + Constant.CHARSET_UTF_8);
        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type");
        response.getWriter().print(str);
    }
}
