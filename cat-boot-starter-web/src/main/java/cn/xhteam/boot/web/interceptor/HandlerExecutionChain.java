package cn.xhteam.boot.web.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HandlerExecutionChain {
    public static final Logger logger = LoggerFactory.getLogger(HandlerExecutionChain.class);
    private final List<HandlerInterceptor> interceptorList;
    private final Object handler;
    private int interceptorIndex;

    public HandlerExecutionChain(Object handler, List<HandlerInterceptor> interceptorList) {
        this.interceptorList = new ArrayList<>();
        this.interceptorIndex = -1;
        if (handler instanceof HandlerExecutionChain) {
            HandlerExecutionChain originalChain = (HandlerExecutionChain) handler;
            this.handler = originalChain.getHandler();
            this.interceptorList.addAll(originalChain.interceptorList);
        } else {
            this.handler = handler;
        }
        this.interceptorList.addAll(interceptorList);
    }

    public Object getHandler() {
        return this.handler;
    }

    public void addInterceptors(HandlerInterceptor... interceptors) {
        this.interceptorList.addAll(Arrays.asList(interceptors));
    }

    public HandlerInterceptor[] getInterceptors() {
        return !this.interceptorList.isEmpty() ? this.interceptorList.toArray(new HandlerInterceptor[0]) : null;
    }

    public boolean applyPreHandle(HttpServletRequest request, HttpServletResponse response)  {
        for (int i = 0; i < this.interceptorList.size(); this.interceptorIndex = i++) {
            HandlerInterceptor interceptor = this.interceptorList.get(i);
            if (!interceptor.preHandle(request, response, this.handler)) {
                this.triggerAfterCompletion(request, response, null);
                return false;
            }
        }
        return true;
    }

    void triggerAfterCompletion(HttpServletRequest request, HttpServletResponse response, Exception ex) {
        for (int i = this.interceptorIndex; i >= 0; --i) {
            HandlerInterceptor interceptor = this.interceptorList.get(i);
            try {
                interceptor.afterCompletion(request, response, this.handler, ex);
            } catch (Throwable var7) {
                logger.error("HandlerInterceptor.afterCompletion threw exception", var7);
            }
        }

    }


    public void applyPostHandle(HttpServletRequest request, HttpServletResponse response) {
        HandlerInterceptor[] interceptors = getInterceptors();
        if (null != interceptors) {
            for (int i = interceptors.length - 1; i >= 0; i--) {
                HandlerInterceptor interceptor = interceptors[i];
                interceptor.postHandle(request, response, this.handler);
            }
        }
    }
}
