package cn.xhteam.boot.web.util;

import cn.xhteam.boot.core.bean.HandContainer;
import cn.xhteam.boot.core.util.BeanUtil;
import cn.xhteam.boot.core.util.ObjectUtil;
import cn.xhteam.boot.web.core.MultipartFile;
import com.alibaba.fastjson2.JSON;
import com.github.houbb.asm.tool.reflection.AsmMethods;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BootUtil {
    public static String lowerFirst(String str) {
        // 同理
        char[] cs = str.toCharArray();
        cs[0] += 32;
        return String.valueOf(cs);
    }

    public static boolean isOrdinaryType(Class<?> cls) {
        List<String> type = Arrays.asList("int", "char", "double", "boolean", "float", "String", "Integer", "Boolean", "Double", "Float");
        return type.contains(cls.getSimpleName());
    }

    public static boolean isParameterTypeObject(Parameter[] parameters) {
        for (Parameter parameter : parameters) {
            if (!isOrdinaryType(parameter.getType())) {
                return true;
            }
        }
        return false;
    }


    //验证参数是否合法
    public static boolean validBaseParam(Parameter[] parameters) {
        boolean isTo = false;
        if (parameters.length > 0) {
            if (!isOrdinaryType(parameters[0].getType())) {
                return false;
            }
            for (Parameter parameter : parameters) {
                if (isTo != isOrdinaryType(parameter.getType())) {
                    throw new RuntimeException("接口参数类型不合规");
                }
            }
        }
        return isTo;
    }

    public static Object[] setBaseParam(Map<String, String[]> mapParam, String[] paramNames, Parameter[] parameters) {
        Object[] paramObj = new Object[paramNames.length];
        for (int i = 0; i < mapParam.keySet().size(); i++) {
            String args = mapParam.get(paramNames[i])[0];
            paramObj[i] = cast(args, parameters[i].getType());
        }
        return paramObj;
    }

    public static Object cast(Object val, Class<?> cls) {
        String simpleName = cls.getSimpleName();
        switch (simpleName) {
            case "Integer":
                return Integer.valueOf((String) val);
            case "Long":
                return Long.valueOf((String) val);
            case "Boolean":
                return Boolean.valueOf((String) val);
            case "Float":
                return Float.valueOf((String) val);
            case "Double":
                return Double.valueOf((String) val);
        }
        return cls.cast(val);
    }

    //根据类型设置参数
    public static Object setParamObject(Map<String, String[]> mapParam, Class<?> cls) {
        try {
            Object o = cls.newInstance();
            for (String key : mapParam.keySet()) {
                setFieldValue(o, key, mapParam.get(key)[0]);
            }
            return o;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static <T> void setFieldValue(T object, String key, String value) throws NoSuchFieldException, IllegalAccessException {
        if (object instanceof Map) {
            Map.class.cast(object).put(key, value);
        } else if (object instanceof List) {
            List.class.cast(object).set(Integer.parseInt(key), value);
        } else {
            Field field = object.getClass().getDeclaredField(key);
            field.setAccessible(true);
            field.set(object, cast(value, field.getType()));
        }
    }

    public static Object[] setMethodMultipartFileBody(HttpServletRequest request, Parameter[] parameters) throws IOException, InstantiationException, IllegalAccessException {
        if (parameters.length > 1) {//对象超过1个参数
            throw new RuntimeException("接口参数对象个数不合法");
        }
        Object[] paramObj = new Object[1];
        MultipartConfigElement newConfig = new MultipartConfigElement(
                request.getLocalAddr(),
                1024 * 1024 * 20,    // 20 MB，修改为新的文件大小限制
                1024 * 1024 * 100,   // 100 MB，修改为新的请求大小限制
                2
        );
        request.getServletContext().setAttribute("javax.servlet.multipartConfig", newConfig);
        Map<String, List<FileItem>> multipart = new ServletFileUpload(new DiskFileItemFactory()).parseParameterMap(request);
        HashMap<String, Object> map = new HashMap<>();
        for (List<FileItem> items : multipart.values()) {
            for (FileItem item : items) {
                if (item.isFormField()) {
                    map.put(item.getFieldName(), item.getString());
                } else {
                    map.put(item.getFieldName(), new MultipartFile(item));
                }
            }
        }
        Object newClass = parameters[0].getType().newInstance();
        Field[] fields = parameters[0].getType().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                field.set(newClass, cast(map.get(field.getName()), field.getType()));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        paramObj[0] = newClass;
        return paramObj;
    }

    public static Object[] setMethodParamByBody(String body, Parameter[] parameters) {
        if (parameters.length > 1) {//对象超过1个参数
            throw new RuntimeException("接口参数对象个数不合法");
        }
        Object object = null;
        if (JSON.isValidArray(body)) {
            object = JSON.parseArray(body, parameters[0].getType());
        } else if (JSON.isValidObject(body)) {
            object = JSON.parseObject(body, parameters[0].getType());
        } else {
            throw new RuntimeException("body不为json数据");
        }
        return new Object[]{object};
    }

    public static Object[] getMethodParamsObj(Map<String, String[]> mapParam, Parameter[] parameters) {
        if (ObjectUtil.isNull(parameters) || parameters.length == 0) {
            return null;
        }
        if (parameters.length > 1) {//对象超过1个参数
            throw new RuntimeException("参数对象存在多个不合法");
        }
        if ("List".equals(parameters[0].getType().getSimpleName())) {
            throw new RuntimeException("GET请求参数不能为集合");
        }
        Parameter parameter = parameters[0];
        Object object = setParamObject(mapParam, parameter.getType());
        return new Object[]{object};
    }

    public static String[] getMethodParams(Method method) {
        String[] paramName = new String[method.getParameterCount()];
        List<String> paramNames = AsmMethods.getParamNamesByAsm(method);
        for (int i = 0; i < paramNames.size(); i++) {
            paramName[i] = paramNames.get(i);
            if (i == method.getParameterCount() - 1) {
                break;
            }
        }
        return paramName;
    }
}
