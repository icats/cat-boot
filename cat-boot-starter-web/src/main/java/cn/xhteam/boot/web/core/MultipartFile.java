package cn.xhteam.boot.web.core;

import org.apache.tomcat.util.http.fileupload.FileItem;

import java.io.IOException;
import java.io.Serializable;

public class MultipartFile implements Serializable {
    private static final long serialVersionUID = 1L;
    private String fileName;
    private byte[] bytes;
    private long size;

    public MultipartFile() {
    }

    public MultipartFile(FileItem fileItem) throws IOException {
        this.fileName = fileItem.getName();
        this.bytes = fileItem.get();
        this.size = fileItem.getSize();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
