package cn.xhteam.boot.web.server;

import cn.xhteam.boot.core.bean.HandContainer;
import cn.xhteam.boot.web.core.DispatcherServlet;
import org.apache.catalina.Context;
import org.apache.catalina.Host;
import org.apache.catalina.Wrapper;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.concurrent.ConcurrentHashMap;

public class WebServerManager {
    private WebServer webServer;
    private String protocol = "org.apache.coyote.http11.Http11NioProtocol";

    private String contextPath = "";

    public WebServerManager(int port) {
        this.webServer = this.getWebServer(port);
    }

    public void start() {
        this.webServer.start();
    }

    public void stop() {
        this.webServer.stop();
    }

    public void destroySilently() {
        this.webServer.destroySilently();
    }

    protected final File createTempDir(String prefix, int port) {
        try {
            File tempDir = Files.createTempDirectory(prefix + "." + port + ".").toFile();
            tempDir.deleteOnExit();
            return tempDir;
        } catch (IOException var4) {
            throw new RuntimeException("Unable to create tempDir. java.io.tmpdir is set to " + System.getProperty("java.io.tmpdir"), var4);
        }
    }

    public WebServer getWebServer(int port) {
        System.setProperty("org.graalvm.nativeimage.imagecode", "false");
        Tomcat tomcat = new Tomcat();
        File docBase = this.createTempDir("tomcat-docbase", port);
        Connector connector = new Connector(this.protocol);
        tomcat.setBaseDir(docBase.getAbsolutePath());
        tomcat.getService().addConnector(connector);
        connector.setThrowOnFailure(true);
        connector.setPort(port);
        tomcat.setConnector(connector);
        tomcat.getHost().setAutoDeploy(false);
        Host host = tomcat.getHost();
        Context context = tomcat.addContext(host, contextPath, docBase.getAbsolutePath());
        context.setRequestCharacterEncoding(StandardCharsets.UTF_8.displayName());
        context.setResponseCharacterEncoding(StandardCharsets.UTF_8.displayName());
        if (context instanceof StandardContext) {
            Wrapper wrapper = tomcat.addServlet(contextPath, DispatcherServlet.class.getName(), new DispatcherServlet());
            this.addMappings(wrapper);
        }

        return new TomcatWebServer(tomcat);
    }
    protected void addMappings(Wrapper wrapper) {
        wrapper.addMapping("/");
        ConcurrentHashMap<String, Method> mappings = HandContainer.getMapping();
        for (String mapping : mappings.keySet()) {
            wrapper.addMapping(mapping);
        }

    }
}
