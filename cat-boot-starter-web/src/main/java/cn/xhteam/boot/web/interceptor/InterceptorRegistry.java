package cn.xhteam.boot.web.interceptor;

import java.util.ArrayList;
import java.util.List;

public class InterceptorRegistry {
    private final List<InterceptorRegistration> registrations = new ArrayList<>();

    public InterceptorRegistry() {
    }

    public List<InterceptorRegistration> getRegistrations() {
        return registrations;
    }

    public InterceptorRegistration addInterceptor(HandlerInterceptor interceptor) {
        InterceptorRegistration registration = new InterceptorRegistration(interceptor);
        this.registrations.add(registration);
        return registration;
    }

}
