package cn.xhteam.boot.web.interceptor;


import javax.servlet.http.HttpServletRequest;

public interface HandlerMapping {
    HandlerExecutionChain getHandler(HttpServletRequest request) ;
}
