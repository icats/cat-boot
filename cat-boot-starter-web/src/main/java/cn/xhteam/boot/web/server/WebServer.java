package cn.xhteam.boot.web.server;

public interface WebServer {
    void start();

    void stop();

    void destroySilently();

    int getPort();

}
