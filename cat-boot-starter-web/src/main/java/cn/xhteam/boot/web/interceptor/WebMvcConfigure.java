package cn.xhteam.boot.web.interceptor;

public interface WebMvcConfigure {
    void addInterceptors(InterceptorRegistry registry);
}
