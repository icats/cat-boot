package cn.xhteam.boot.web.interceptor;

import java.nio.file.PathMatcher;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InterceptorRegistration {
    private final HandlerInterceptor interceptor;
    private final List<String> includePatterns = new ArrayList();
    private final List<String> excludePatterns = new ArrayList();
    private PathMatcher pathMatcher;

    public InterceptorRegistration(HandlerInterceptor interceptor) {
        this.interceptor = interceptor;
    }

    public InterceptorRegistration addPathPatterns(String... patterns) {
        this.includePatterns.addAll(Arrays.asList(patterns));
        return this;
    }

    public InterceptorRegistration excludePathPatterns(String... patterns) {
        this.excludePatterns.addAll(Arrays.asList(patterns));
        return this;
    }

    public InterceptorRegistration pathMatcher(PathMatcher pathMatcher) {
        this.pathMatcher = pathMatcher;
        return this;
    }

    public Object getInterceptor() {
        if (this.includePatterns.isEmpty() && this.excludePatterns.isEmpty()) {
            return this.interceptor;
        } else {
            String[] include = this.includePatterns.toArray(new String[0]);
            String[] exclude = this.excludePatterns.toArray(new String[0]);
            MappedInterceptor mappedInterceptor = new MappedInterceptor(include, exclude, this.interceptor);
            if (this.pathMatcher != null) {
                mappedInterceptor.setPathMatcher(this.pathMatcher);
            }
            return mappedInterceptor;
        }
    }
}
