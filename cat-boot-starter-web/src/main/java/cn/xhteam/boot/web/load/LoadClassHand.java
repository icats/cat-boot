package cn.xhteam.boot.web.load;

import cn.xhteam.boot.core.bean.HandContainer;
import cn.xhteam.boot.core.enums.HttpMethod;
import cn.xhteam.boot.core.stereotype.*;
import cn.xhteam.boot.core.util.BeanUtil;
import cn.xhteam.boot.core.util.Constant;
import cn.xhteam.boot.core.util.FileClassUtil;
import cn.xhteam.boot.core.util.StringUtil;
import cn.xhteam.boot.web.CatBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class LoadClassHand {
    private final List<Class<?>> beans = new ArrayList<>();//所有需要托管的bean
    private final List<Class<?>> initializationBeans = new ArrayList<>();//需要自行启动的配置bean
    public static final Logger logger = LoggerFactory.getLogger(LoadClassHand.class);

    private void initialization() throws InstantiationException, IllegalAccessException, InvocationTargetException {
        synchronized (this) {
            loadBeanContainer();//加载所有的bean
            initializationBean();//初始化启动的bean（比如mybatis mapper注入）
            injectAutoBean();//自动注入bean
            loadBeanCache();//处理二级缓存
        }
    }

    private LoadClassHand(Class<?> bootClass) {
        String bootPackage = CatBootApplication.class.getPackage().getName();
        bootPackage = bootPackage.substring(0, bootPackage.lastIndexOf("."));
        List<Class<?>> list = FileClassUtil.getClasses(bootPackage);//寻找包内的
        list.addAll(FileClassUtil.getClasses(bootClass.getPackage().getName()));//扫描外部的类
        for (Class<?> cls : list) {
            if (cls.isAnnotationPresent(Service.class)) {
                this.beans.add(0, cls);
            }
            if (cls.isAnnotationPresent(Configuration.class)) {
                this.beans.add(0, cls);
            }
            if (cls.isAnnotationPresent(Controller.class)) {
                this.beans.add(cls);
            }
            if (cls.isAnnotationPresent(Initialized.class)) {
                this.initializationBeans.add(cls);
            }
        }
    }

    //处理二级缓存
    private void loadBeanCache() throws InstantiationException, IllegalAccessException {
        for (String key : HandContainer.getBeanCaches().keySet()) {
            Class<?> obj = HandContainer.getBeanCache(key);
            initializationObject(obj, key);
        }
    }

    private void initializationBean() throws InvocationTargetException, IllegalAccessException, InstantiationException {
        for (Class<?> t : initializationBeans) {
            for (Method method : t.getMethods()) {
                if (!method.isAnnotationPresent(Bean.class)) {
                    continue;
                }
                method.invoke(t.newInstance());
            }
        }
    }

    private void loadBeanContainer() throws InstantiationException, IllegalAccessException {
        for (Class<?> classObj : beans) {
            if (classObj.isAnnotationPresent(Service.class)) {
                Service service = classObj.getAnnotation(Service.class);
                addServiceBean(classObj, service.value());
            }
            if (classObj.isAnnotationPresent(Configuration.class)) {
                addConfigurationBean(classObj);
            }
            if (classObj.isAnnotationPresent(Controller.class)) {
                Controller controller = classObj.getAnnotation(Controller.class);
                addMappings(StringUtil.addSlashMissing(controller.value()), classObj.getDeclaredMethods());
            }
            logger.info("scan bean loader：" + classObj.getTypeName());
        }
    }

    private void addServiceBean(Class<?> classObj, String beanName) throws InstantiationException, IllegalAccessException {
        Class<?>[] interfaces = classObj.getInterfaces();
        for (Class<?> obj : interfaces) {
            String classBeanName = BeanUtil.getClassBeanName(obj, beanName);
            initializationObject(classObj, classBeanName);
        }
    }

    //判断类是否已经全部在容器初始化了，如果没有需要放置在二级缓存中
    private void initializationObject(Class<?> classObj, String classBeanName) throws InstantiationException, IllegalAccessException {
        Object newClass = classObj.newInstance();
        Field[] fields = classObj.getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAnnotationPresent(Autowired.class)) {
                continue;
            }
            Autowired autowired = field.getAnnotation(Autowired.class);
            String beanName = Objects.nonNull(autowired) ? autowired.value() : null;
            //bean不在容器中，需要放置在二级缓存中
            if (!HandContainer.containsKey(BeanUtil.getClassBeanName(field.getType(), beanName))) {
                if (!classObj.isAnnotationPresent(Controller.class)) {
                    //控制层放在最后
                }
                HandContainer.putBeanCacheMap(classBeanName, classObj);
                return;
            }
            field.setAccessible(true);
            try {
                field.set(newClass, HandContainer.getBeanMap(BeanUtil.getClassBeanName(field.getType(), beanName)));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        HandContainer.putBeanMap(classBeanName, newClass);
    }

    private void injectAutoBean() throws InstantiationException, IllegalAccessException {
        for (Class<?> classObj : beans) {
            initializationObject(classObj, BeanUtil.getClassBeanName(classObj));
        }
    }

    private void addConfigurationBean(Class<?> clsObj) throws InstantiationException, IllegalAccessException {
        Method[] methods = clsObj.getDeclaredMethods();
        Object obj = clsObj.newInstance();
        for (Method method : methods) {
            if (!method.isAnnotationPresent(Bean.class)) {
                continue;
            }
            try {
                Object[] objects = getContainerParameter(method.getParameters());
                Object clsInst;
                if (objects.length > 0) {
                    clsInst = method.invoke(obj, objects);
                } else {
                    clsInst = method.invoke(obj);
                }
                if (Objects.nonNull(clsInst)) {
                    HandContainer.putBeanMap(BeanUtil.getClassBeanName(clsInst.getClass()), clsInst);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private Object[] getContainerParameter(Parameter[] parameters) {
        Object[] objects = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            if (HandContainer.containsKey(BeanUtil.getParameterBeanName(parameters[i]))) {
                objects[i] = HandContainer.getBeanMap(BeanUtil.getParameterBeanName(parameters[i]));
            } else {
                throw new RuntimeException(BeanUtil.getParameterBeanName(parameters[i]) + ":参数对象不存在容器");
            }
        }
        return objects;
    }

    private void addMappings(String path, Method[] methods) {
        for (Method method : methods) {
            if (method.isAnnotationPresent(RequestMapping.class)) {
                RequestMapping mapping = method.getAnnotation(RequestMapping.class);
                for (HttpMethod httpMethod : HttpMethod.values()) {
                    addMapping(method, httpMethod.name() + Constant.textLine + path + StringUtil.addSlashMissing(mapping.value()));
                }
            }
            if (method.isAnnotationPresent(GetMapping.class)) {
                GetMapping mapping = method.getAnnotation(GetMapping.class);
                addMapping(method, HttpMethod.GET.name() + Constant.textLine + path + StringUtil.addSlashMissing(mapping.value()));
            }
            if (method.isAnnotationPresent(PostMapping.class)) {
                PostMapping mapping = method.getAnnotation(PostMapping.class);
                addMapping(method, HttpMethod.POST.name() + Constant.textLine + path + StringUtil.addSlashMissing(mapping.value()));
            }
        }
    }

    private void addMapping(Method method, String value) {
        if (HandContainer.containsMappingKey(value)) {
            throw new RuntimeException("重复mapping地址：" + value + "\t方法名：" + method.getName());
        }
        HandContainer.putMethod(value, method);
    }

    public static void loadBean(Class<?> bootClass) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        new LoadClassHand(bootClass).initialization();
    }
}
