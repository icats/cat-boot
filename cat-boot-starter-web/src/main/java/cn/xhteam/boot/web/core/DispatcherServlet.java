package cn.xhteam.boot.web.core;

import cn.xhteam.boot.core.bean.HandContainer;
import cn.xhteam.boot.core.enums.HttpMethod;
import cn.xhteam.boot.core.util.BeanUtil;
import cn.xhteam.boot.core.util.Constant;
import cn.xhteam.boot.web.CatBootApplication;
import cn.xhteam.boot.web.interceptor.*;
import cn.xhteam.boot.web.server.FrameworkServlet;
import cn.xhteam.boot.web.util.BootUtil;
import cn.xhteam.boot.web.util.ServletUtils;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class DispatcherServlet extends FrameworkServlet {
    public static final Logger logger = LoggerFactory.getLogger(DispatcherServlet.class);
    private static List<HandlerMapping> handlerMappings;

    public DispatcherServlet() {
    }


    public String getRequestBody(ServletInputStream ris) throws IOException {
        StringBuilder content = new StringBuilder();
        byte[] b = new byte[1024];
        int lens;
        while ((lens = ris.read(b)) > 0) {
            content.append(new String(b, 0, lens));
        }
        return content.toString();// 内容
    }

    public Object[] getRequestParam(HttpServletRequest request, Method method) throws IOException, InstantiationException, IllegalAccessException {
        if (method.getParameterCount() == 0) {
            return null;
        }
        Object[] objects = new Object[method.getParameterCount()];
        Parameter[] parameters = method.getParameters();
        if (HttpMethod.GET.name().equalsIgnoreCase(request.getMethod())) {
            if (BootUtil.isParameterTypeObject(parameters)) { //验证参数是否为基础数据类型 true 对象类型，false 基本类型
                objects = BootUtil.getMethodParamsObj(request.getParameterMap(), parameters);
            } else {
                objects = BootUtil.setBaseParam(request.getParameterMap(), BootUtil.getMethodParams(method), parameters);
            }
        }
        if (HttpMethod.POST.name().equalsIgnoreCase(request.getMethod())) {
            //body
            if (request.getContentType().startsWith(Constant.CONTENT_APPLICATION_JSON)) {
                String body = getRequestBody(request.getInputStream());
                objects = BootUtil.setMethodParamByBody(body, parameters);
            }
            //form表单
            if (request.getContentType().startsWith(Constant.CONTENT_MULTIPART_FORM_DATA) || request.getContentType().startsWith(Constant.CONTENT_APPLICATION_FORM_DATA) || request.getContentType().startsWith(Constant.CONTENT_APPLICATION_FORM_URLENCODED)) {
                objects = BootUtil.setMethodMultipartFileBody(request, parameters);
            }
        }
        return objects;
    }

    protected HandlerExecutionChain getHandler(HttpServletRequest request) {
        if (handlerMappings != null) {
            for (HandlerMapping mapping : handlerMappings) {
                HandlerExecutionChain handler = mapping.getHandler(request);
                if (handler != null) {
                    return handler;
                }
            }
        }
        return null;
    }

    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String path = request.getRequestURI();
        PrintWriter out = null;
        response.setContentType(Constant.CONTENT_TEXT_HTML + Constant.CHARSET_UTF_8);

        InterceptorRegistry interceptorRegistry = (InterceptorRegistry) HandContainer.getBeanMap(BeanUtil.getClassBeanName(InterceptorRegistry.class));
        for (InterceptorRegistration registration : interceptorRegistry.getRegistrations()) {
            HandlerInterceptor handlerInterceptor = (HandlerInterceptor) registration.getInterceptor();
            handlerInterceptor.preHandle(request, response, null);
        }

        try {
            out = response.getWriter();
            Method method = HandContainer.getMethod(request.getMethod() + Constant.textLine + path);
            if (method != null) {
                Object[] args = getRequestParam(request, method);
                String beanName = method.getDeclaringClass().getTypeName();
                Object bean = HandContainer.getBeanMap(beanName);
                Object object = null == args || args.length == 0 ? method.invoke(bean) : method.invoke(bean, args);
                String json = JSON.toJSONString(object, JSONWriter.Feature.WriteMapNullValue);
                ServletUtils.renderStr(response, json);
            } else {
                String html = this.doHome();
                if (!"/".equals(path)) {
                    html = Constant.RESPONSE_404_HTML_EN;
                    response.setStatus(404);
                }
                ServletUtils.renderHtml(response, html);
            }
        } catch (Exception e) {
            assert out != null;
            response.setStatus(500);
            ServletUtils.renderStr(response, e.getMessage());
            e.printStackTrace();
        }
    }


    @Override
    protected void doService(HttpServletRequest request, HttpServletResponse response) throws IOException {
        this.doDispatch(request, response);
    }
}
