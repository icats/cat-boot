package cn.xhteam.boot.web.configuration;
import cn.xhteam.boot.core.stereotype.Configuration;
import cn.xhteam.boot.web.interceptor.InterceptorRegistry;
import cn.xhteam.boot.web.interceptor.WebMvcConfigure;
@Configuration
public class MyConfig implements WebMvcConfigure {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyInterceptor())
                .addPathPatterns("/**")  //所有请求都被拦截包括静态资源
                .excludePathPatterns("/", "/login", "/css/**", "/fonts/**", "/images/**", "/js/**"); //放行的请求
    }

}
