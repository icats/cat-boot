package cn.xhteam.boot.web.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.PathMatcher;

public final class MappedInterceptor implements HandlerInterceptor {
    private final String[] includePatterns;
    private final String[] excludePatterns;
    private final HandlerInterceptor interceptor;
    private PathMatcher pathMatcher;

    public MappedInterceptor(String[] includePatterns, HandlerInterceptor interceptor) {
        this(includePatterns, (String[]) null, (HandlerInterceptor) interceptor);
    }

    public MappedInterceptor(String[] includePatterns, String[] excludePatterns, HandlerInterceptor interceptor) {
        this.includePatterns = includePatterns;
        this.excludePatterns = excludePatterns;
        this.interceptor = interceptor;
    }

    public void setPathMatcher(PathMatcher pathMatcher) {
        this.pathMatcher = pathMatcher;
    }

    public PathMatcher getPathMatcher() {
        return this.pathMatcher;
    }

    public String[] getPathPatterns() {
        return this.includePatterns;
    }

    public HandlerInterceptor getInterceptor() {
        return this.interceptor;
    }


    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        return this.interceptor.preHandle(request, response, handler);
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        this.interceptor.postHandle(request, response, handler);
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        this.interceptor.afterCompletion(request, response, handler, ex);
    }
}
