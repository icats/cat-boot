package cn.xhteam.boot.web.core;

import cn.xhteam.boot.core.properties.PropertiesConfig;
import cn.xhteam.boot.core.util.Constant;
import cn.xhteam.boot.core.util.StringUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class PrintBanner extends Constant {

    public static void loadBanner(Class<?> BootClass) {
        try {
            InputStream inputStream = BootClass.getResourceAsStream(banner + txtSuffix);
            if (null == inputStream) {
                inputStream = BootClass.getResourceAsStream(banner+defaultBanner+txtSuffix);
            }
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while (true) {
                assert inputStream != null;
                if ((length = inputStream.read(buffer)) == -1) break;
                result.write(buffer, 0, length);
            }
            String str = result.toString(StandardCharsets.UTF_8.name()).replace("${cat-boot.version}", PropertiesConfig.getProperty("cat-boot.version"));
            System.out.println(str);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
