//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.xhteam.boot.web.server;

import cn.xhteam.boot.web.util.Assert;
import org.apache.catalina.Container;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.util.ServerInfo;
import org.apache.naming.ContextBindings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.util.concurrent.atomic.AtomicInteger;

public class TomcatWebServer implements WebServer {
    public static final Logger logger = LoggerFactory.getLogger(TomcatWebServer.class);
    private final Object monitor;
    private static final AtomicInteger containerCounter = new AtomicInteger(-1);
    private volatile boolean started;
    private final boolean autoStart;
    private final Tomcat tomcat;

    public TomcatWebServer(Tomcat tomcat) {
        this(tomcat, true);
    }

    public TomcatWebServer(Tomcat tomcat, boolean autoStart) {
        this.monitor = new Object();
        Assert.notNull(tomcat, "Tomcat Server must not be null", null);
        this.tomcat = tomcat;
        this.autoStart = autoStart;
        this.initialize();
    }

    public void start() {
        synchronized (this.monitor) {
            if (!this.started) {
                boolean var9 = false;
                Context context;
                label73:
                {
                    try {
                        var9 = true;
                        Connector connector = this.tomcat.getConnector();
                        this.started = true;
                        logger.info("Tomcat started on port(s): with context path ");
                        var9 = false;
                        break label73;
                    } catch (Exception var10) {
                        var9 = false;
                    } finally {
                        if (var9) {
                            context = this.findContext();
                            ContextBindings.unbindClassLoader(context, context.getNamingToken(), this.getClass().getClassLoader());
                        }
                    }

                    context = this.findContext();
                    ContextBindings.unbindClassLoader(context, context.getNamingToken(), this.getClass().getClassLoader());
                    return;
                }

                context = this.findContext();
                ContextBindings.unbindClassLoader(context, context.getNamingToken(), this.getClass().getClassLoader());
            }
        }
    }

    private void initialize() {
        logger.info("Starting Servlet engine：\t{}", ServerInfo.getServerInfo());
        synchronized (this.monitor) {
            try {
                Context context = this.findContext();
                context.addLifecycleListener((event) -> {
                    if (context.equals(event.getSource()) && "start".equals(event.getType())) {
                    }

                });
                this.tomcat.start();

                try {
                    ContextBindings.bindClassLoader(context, context.getNamingToken(), this.getClass().getClassLoader());
                } catch (NamingException var5) {
                }

                this.startDaemonAwaitThread();
            } catch (Exception var6) {
                this.destroySilently();
                throw new RuntimeException("Unable to start embedded Tomcat", var6);
            }

        }
    }

    private Context findContext() {
        Container[] var1 = this.tomcat.getHost().findChildren();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            Container child = var1[var3];
            if (child instanceof Context) {
                return (Context) child;
            }
        }

        throw new IllegalStateException("The host does not contain a Context");
    }

    private void stopTomcat() throws LifecycleException {
        this.tomcat.stop();
    }

    public void stop() {
        synchronized (this.monitor) {
            boolean wasStarted = this.started;

            try {
                this.started = false;

                try {
                    this.stopTomcat();
                    this.tomcat.destroy();
                } catch (LifecycleException var10) {
                }
            } catch (Exception var11) {
                throw new RuntimeException("Unable to stop embedded Tomcat", var11);
            } finally {
                if (wasStarted) {
                    containerCounter.decrementAndGet();
                }

            }

        }
    }

    public void destroySilently() {
        try {
            this.tomcat.destroy();
        } catch (LifecycleException ignored) {
        }

    }

    private void startDaemonAwaitThread() {
        Thread awaitThread = new Thread("container-" + containerCounter.get()) {
            public void run() {
                TomcatWebServer.this.tomcat.getServer().await();
            }
        };
        awaitThread.setContextClassLoader(this.getClass().getClassLoader());
        awaitThread.setDaemon(false);
        awaitThread.start();
    }

    public int getPort() {
        Connector connector = this.tomcat.getConnector();
        return connector != null ? connector.getLocalPort() : -1;
    }
}
