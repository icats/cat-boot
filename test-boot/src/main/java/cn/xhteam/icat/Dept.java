package cn.xhteam.icat;

import lombok.Data;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

@Data
public class Dept {
    private Long id;
    private String name;
    private File file;
}