package cn.xhteam.icat.mapper;

import cn.xhteam.icat.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

public interface UserMapper extends BaseMapper<SysUser> {
    List<SysUser> findAlls(SysUser sysUser);
}
