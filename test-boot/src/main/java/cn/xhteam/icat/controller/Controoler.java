package cn.xhteam.icat.controller;


import cn.xhteam.boot.core.stereotype.Autowired;
import cn.xhteam.boot.core.stereotype.Controller;
import cn.xhteam.boot.core.stereotype.PostMapping;
import cn.xhteam.boot.core.stereotype.RequestMapping;
import cn.xhteam.icat.SysUser;
import cn.xhteam.icat.service.IUserService;
import cn.xhteam.icat.service.TestService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.List;

@Controller
public class Controoler {
    @Autowired
    private IUserService userService;
    @Autowired
    private TestService testService;

    @RequestMapping("/user1")
    public List<SysUser> get(SysUser sysUser) {
        List<SysUser> list = testService.get(sysUser);
        System.out.println(sysUser.toString());
        return list;
    }

    public static void main(String[] args) throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        URL[] urls = {
                new URL("jar:file:/E:/dev/workspace/cat-boot/test-boot/target/test-boot-2.1.0.jar!/BOOT-INF/classes"), // 指向 classes
                new URL("jar:file:/E:/dev/workspace/cat-boot/test-boot/target/test-boot-2.1.0.jar!/BOOT-INF/lib"), // 指向 classes
        };
        URLClassLoader urlClassLoader = new URLClassLoader(urls);
        Class<?> mainClass = urlClassLoader.loadClass("cn.xhteam.icat.Main");
        mainClass.getMethod("main", String[].class).invoke(null, (Object) args);
        System.out.println(urlClassLoader.getURLs());
    }
    @RequestMapping("/save")
    public String save(SysUser sysUser) {
        testService.saveUser(sysUser);
        return "111";
    }

//    @RequestMapping("/test4")
//    public String test4(SysUser sysUser) {
//        System.out.println(sysUser);
//        MultipartFile file = sysUser.getFile();
//        String targetFilePath = "F:\\test" + File.separator + file.getFileName();
//        FileUtil.writeBytes(file.getBytes(), targetFilePath);
//        return "2323";
//    }

    @RequestMapping("/test1")
    public Integer test1(Integer name) {
        return name;
    }

    @RequestMapping("/test2")
    public List<SysUser> test2(List<SysUser> sysUser) {
        return sysUser;
    }

    @PostMapping("/test3")
    public SysUser test3(SysUser sysUser) {
        return sysUser;
    }


}
