package cn.xhteam.icat.service;

import cn.xhteam.boot.core.stereotype.Autowired;
import cn.xhteam.boot.core.stereotype.Service;
import cn.xhteam.boot.mybatis.loader.TransactionManager;
import cn.xhteam.icat.SysUser;
import cn.xhteam.icat.mapper.UserMapper;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IUserService userService;

    @Override
    public void saveUser(SysUser user) {
        userMapper.insert(user);
    }

    @Override
    public List<SysUser> get(SysUser user) {
        return userService.say(user);
    }
}
