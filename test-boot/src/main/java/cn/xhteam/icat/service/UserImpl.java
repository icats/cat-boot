package cn.xhteam.icat.service;


import cn.xhteam.boot.core.stereotype.Autowired;
import cn.xhteam.boot.core.stereotype.Service;
import cn.xhteam.icat.SysUser;
import cn.xhteam.icat.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<SysUser> say(SysUser sysUser) {
        return userMapper.selectList(new LambdaQueryWrapper<>());
    }
}