package cn.xhteam.icat.service;

import cn.xhteam.icat.SysUser;

import java.util.List;

public interface TestService {
    void saveUser(SysUser user);

    List<SysUser> get(SysUser user);
}
