package cn.xhteam.plugin;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.jar.*;
import java.util.stream.Stream;

@Mojo(
        name = "repackage",
        defaultPhase = LifecyclePhase.PACKAGE,
        threadSafe = true,
        requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME,
        requiresDependencyCollection = ResolutionScope.COMPILE_PLUS_RUNTIME
)
public class RunMainPlugin extends AbstractMojo {
    //输出的jar
    @Parameter(defaultValue = "${project.build.directory}/${project.artifactId}-${project.version}.jar")
    private File outputJar;
    //lib包
    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;
    //class
    @Parameter(defaultValue = "${project.build.outputDirectory}", readonly = true, required = true)
    private File classesDirectory;

    @Parameter(defaultValue = "${project.build.directory}")
    private File directory;

    private final static String BOOT_CLASSES = "BOOT-INF/classes";
    private final static String BOOT_LIB = "BOOT-INF/lib/";
    private String mainClassPackage = "cn.xhteam.icat.Main";
    private String classLibPath = null;

    @Override
    public void execute() throws MojoExecutionException {
        try {
            createJar();
        } catch (IOException e) {
            throw new MojoExecutionException("Error creating jar file", e);
        }
    }

    private void addStartClass(JarOutputStream jos) throws IOException {
        File pluginJarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        String pluginJarFileName = pluginJarFile.getName();
        String pluginFileAbsolutePath = pluginJarFile.getAbsolutePath().replace(pluginJarFileName, "classes\\");
        addDirectoryToMain(new File(pluginFileAbsolutePath), pluginFileAbsolutePath, jos);
    }

    private void addDirectoryToMain(File source, String path, JarOutputStream jos) throws IOException {
        File[] files = source.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    addDirectoryToMain(file, path, jos);
                } else if (file.isFile()) {
                    String entryName = file.getAbsolutePath();
                    if (!entryName.endsWith(".class")) {
                        continue;
                    }
                    jos.putNextEntry(new JarEntry(entryName.replace(path, "").replace("\\", "/")));
                    Files.copy(file.toPath(), jos);
                }
            }
        }
    }


    private void addDirectoryToJar(File source, JarOutputStream jos) throws IOException {
        File[] files = source.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    addDirectoryToJar(file, jos);
                } else if (file.isFile()) {
                    //设置主类
                    setMainClassPackage(file);
                    String entryName = file.getAbsolutePath().replace(classesDirectory.getAbsolutePath(), "").replace("\\", "/");
                    jos.putNextEntry(new JarEntry(BOOT_CLASSES + entryName));
                    Files.copy(file.toPath(), jos);
                }
            }
        }
    }

    private void packageDependencies(JarOutputStream jos) throws IOException {
        StringBuilder classPath = new StringBuilder();
        for (Artifact artifact : project.getArtifacts()) {
            File file = artifact.getFile();
            if (file != null && file.exists()) {
                Path dependency = file.toPath();
                jos.putNextEntry(new JarEntry(BOOT_LIB + dependency.getFileName().toString()));
                classPath.append(BOOT_LIB).append(dependency.getFileName().toString()).append(" ");
                Files.copy(dependency, jos);
            }
        }
        classLibPath = classPath.toString();
    }


    private void createJar() throws IOException {
        try (JarOutputStream jarOutputStream = new JarOutputStream(Files.newOutputStream(outputJar.toPath()), createManifest())) {
            //依赖包添加
            packageDependencies(jarOutputStream);
            //程序添加
            addDirectoryToJar(classesDirectory, jarOutputStream);
            //启动类
            addStartClass(jarOutputStream);
        }
        getLog().info("恭喜您，成功打包，打包后存放jar地址: " + outputJar.getAbsolutePath());
    }


    private Manifest createManifest() {
        Manifest manifest = new Manifest();
        manifest.getMainAttributes().putValue("Manifest-Version", "1.0");
        manifest.getMainAttributes().putValue("Main-Class", "cn.xhteam.plugin.Jarlauncher");
        manifest.getMainAttributes().putValue("Start-Class", mainClassPackage);
        manifest.getMainAttributes().putValue("CAT-BOOT-Classes", BOOT_CLASSES);
        manifest.getMainAttributes().putValue("CAT-BOOT-Version", "2.1.0");
        return manifest;
    }

    private void setMainClassPackage(File file) {
        if (null == mainClassPackage) {
            if (containsString(file, "CatBootApplication.run")) {
                // 获取相对路径
                String relativePath = file.getPath();
                System.out.println("相对路径: " + relativePath);

                // 获取绝对路径
                String absolutePath = file.getAbsolutePath();
                System.out.println("绝对路径: " + absolutePath);

                // 获取规范路径（可能会解决符号链接或 ".." 等问题）
                try {
                    String canonicalPath = file.getCanonicalPath();
                    mainClassPackage = canonicalPath;
                    System.out.println("规范路径: " + canonicalPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean containsString(File file, String searchString) {
        try (Stream<String> lines = Files.lines(Paths.get(file.getPath()))) {
            return lines.anyMatch(line -> line.contains(searchString));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false; // 未找到字符串
    }
}
