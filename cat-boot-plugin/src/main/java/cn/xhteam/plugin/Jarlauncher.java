package cn.xhteam.plugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Jarlauncher {

    public static void main(String[] args) {
        try {
            ClassLoader currentClassLoader = Jarlauncher.class.getClassLoader();
            List<URL> jarUrls = getJarUrls();
            URL[] urls = jarUrls.toArray(new URL[0]);
            try (URLClassLoader urlClassLoader = new URLClassLoader(urls, Thread.currentThread().getContextClassLoader())) {
                Thread.currentThread().setContextClassLoader(urlClassLoader);
                String mainClassName = getMainClassName(currentClassLoader);
                if (mainClassName == null) {
                    throw new IllegalStateException("无法从 MANIFEST.MF 中找到 Main-Class 属性");
                }
                Class<?> mainClass = urlClassLoader.loadClass(mainClassName);
                mainClass.getMethod("main", String[].class).invoke(null, (Object) args);
                Thread.currentThread().setContextClassLoader(ClassLoader.getSystemClassLoader());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // 获取当前 JAR 包中的所有 lib 和 classes 资源的 URL
    private static List<URL> getJarUrls() throws IOException, URISyntaxException {
        List<URL> jarUrls = new ArrayList<>();
        URI jarUri = Jarlauncher.class.getProtectionDomain().getCodeSource().getLocation().toURI();
        Path jarPath = Paths.get(jarUri);
        File tempDir = Files.createTempDirectory("extractedJars").toFile();
        // 解压并获取 BOOT-INF/lib 下所有的 JAR 文件
        extractNestedJars(jarPath.toFile(), tempDir.toPath());
        File classesDir = new File(tempDir, "BOOT-INF/classes/");
        jarUrls.add(classesDir.toURI().toURL());
        // 加载解压后的依赖
        File libDir = new File(tempDir, "BOOT-INF/lib/");
        File[] jarFiles = libDir.listFiles((dir, name) -> name.endsWith(".jar"));
        if (jarFiles != null) {
            for (File jar : jarFiles) {
                jarUrls.add(jar.toURI().toURL());
            }
        }
        jarUrls.add(libDir.toURI().toURL());
        return jarUrls;
    }


    public static void extractNestedJars(File jarFile, Path tempDir) throws IOException {
        try (JarFile jar = new JarFile(jarFile)) {
            Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                File file = new File(tempDir.toFile(), entry.getName());
                if (entry.isDirectory()) {
                    file.mkdirs();
                } else {
                    file.getParentFile().mkdirs();
                    try (InputStream is = jar.getInputStream(entry);
                         FileOutputStream fos = new FileOutputStream(file)) {
                        byte[] buffer = new byte[1024];
                        int len;
                        while ((len = is.read(buffer)) != -1) {
                            fos.write(buffer, 0, len);
                        }
                    }
                }
            }
        }
    }

    // 从当前 JAR 包中读取 MANIFEST.MF 文件，并获取 Main-Class
    private static String getMainClassName(ClassLoader classLoader) throws IOException {
        // 通过类加载器读取 META-INF/MANIFEST.MF
        URL manifestUrl = classLoader.getResource("META-INF/MANIFEST.MF");
        if (manifestUrl != null) {
            Manifest manifest = new Manifest(manifestUrl.openStream());
            Attributes attributes = manifest.getMainAttributes();
            return attributes.getValue("Start-Class");
        }
        return null;
    }
}
