<p align="center">
	<strong>It is very convenient for the development of individual microservices, beneficial for the development of web service containers, API interfaces, and zero configuration.</strong>
</p>
<p align="center">
	👉 <a href="http://xhteam.cn">http://xhteam.cn/</a> 👈
</p>

<p align="center">
	<a target="_blank" href="https://search.maven.org/artifact/cn.xhteam/cat-boot">
		<img src="https://img.shields.io/maven-central/v/cn.xhteam/cat-boot.svg?label=Maven%20Central" />
	</a>
	<a target="_blank" href="http://license.coscl.org.cn/MulanPSL2/index.html">
		<img src="https://img.shields.io/:license-MulanPSL2-blue.svg" />
	</a>
	<a target="_blank" href="https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html">
		<img src="https://img.shields.io/badge/JDK-8+-green.svg" />
	</a>
    <a target="_blank" href='https://gitee.com/icats/cat-boot/stargazers'>
		<img src='https://gitee.com/icats/cat-boot/badge/star.svg?theme=gvp' alt='star'/>
	</a>

</p>

<p align="center">
	<a href="https://shang.qq.com/wpa/qunwpa?idkey=211e7ac19d8dbf39d07cee5cc1504f9a9c8cdc4ff5455e1a850886c1c7d7bcbc">
	<img src="https://img.shields.io/badge/QQ%E7%BE%A4-577155838-orange"/></a>
</p>

-------------------------------------------------------------------------------

## 📚简介
cat-boot是一个小而全的微服务框架，它非常方便微服务开发、web服务、IOC容器、集成Redis、Mybatis-puls工具、API接口零配置开发。


## 🛠️包含组件
| 模块 | 介绍                             |
|--|--------------------------------|
| IOC | 控制反转(IOC)是面向对象编程中的一种设计原则，可以用来减少代码之间的耦合度。本质上是把对象创建和对象之间的调用关系交给Cat-Boot来进行管理。                       |
| AOP | AOP面向切面编程，采取CGLib实现动态代理，取代了传统纵向继承体系重复性代码（性能监视、安全检查、缓存）                       |
| 拦截器 | 类实现HandlerInterceptor接口，重写preHandle()，postHandle()，afterCompletion() 三个方法       |
| Web容器服务 | 服务器端的开发。从简单性、可测试性和松耦合的角度而言，服务容器基于Tomcat开发，对Tomcat进行封装 |
| mybatis-plus集成 | 实现对数据库的CRUD操作             |
| Redis集成 | 配置redis连接池，调用RedisTemplate实现   |

-------------------------------------------------------------------------------


## 📦安装

### 🍊Maven
在项目的pom.xml添加parent和dependencies中加入以下内容:

```xml
<parent>
   <artifactId>cat-boot</artifactId>
    <groupId>cn.xhteam.boot</groupId>
    <version>2.1.0</version>
    <relativePath/>
</parent>

<dependency>
    <groupId>cn.xhteam.boot</groupId>
    <artifactId>cat-boot-starter-web</artifactId>
    <version>2.1.0</version>
</dependency>
```

### 📥下载jar

点击以下链接，下载`cat-boot-X.X.X.jar`即可：

- [Maven中央库](https://repo1.maven.org/maven2/cn/xhteam/)

> 🔔️注意
> cat-boot只支持JDK8+，不能保证所有功能方法可用。

### 🚽编译安装
1、 创建application.properties或application.yml文件选一种即可，如果不自定义配置，则使用cat-boot框架默认配置
```yml
#yml配置案例
server:
  port: 9000
catBoot:
  datasource:
    driverClassName: com.mysql.cj.jdbc.Driver
    druid:
      url: jdbc:mysql://127.0.0.1:3306/managersystem
      username: root
      password: 123456
#mybatis-plus配置
mybatis-plus:
  #指定mapper接口类
  mapper-package: cn.xhteam.mapper
  #指定xml文件所在目录
  mapper-locations: mapper
redis:
  host: 127.0.0.1
  port: 6379
  database: 14
  password: 123456
```
```properties
#properties配置案例
server.prot=9000
redis.host=127.0.0.1
redis.port=6379
redis.database=1
redis.password=123456
```
2、在项目根目录创建服务启动类,在main方法中调用CatBootApplication.run(),运行main方法启动服务
```java
package cn.xhteam.boot;
public class Application {
    public static void main(String[] args)  {
        CatBootApplication.run(Application.class,args);
    }
}

```

### 🚽服务器打包部署
1、在pom.xml中配置打包插件，需要注意将mainClass换成自己的启动类
```xml
<build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <configuration>
                    <transformers>
                        <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                            <mainClass>cn.test.Application</mainClass> <!--改成启动类-->
                        </transformer>
                    </transformers>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
```
2、打包/运行命令
```sh
mvn clean install #jar包

java -jar ./XXX.jar #运行jar
```


-------------------------------------------------------------------------------

### 🐞提供bug反馈或建议

提交问题反馈请说明正在使用的JDK版本呢、Cat-Boot版本和相关依赖库版本。

- [Gitee issue](https://gitee.com/icats/cat-boot/issues)

