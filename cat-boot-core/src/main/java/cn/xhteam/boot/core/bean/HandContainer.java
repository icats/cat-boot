//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.xhteam.boot.core.bean;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

public class HandContainer {
    //mapping地址
    private static final ConcurrentHashMap<String, Method> mapping = new ConcurrentHashMap<>();
    //初始化已完成的bean
    private static final ConcurrentHashMap<String, Object> beanMap = new ConcurrentHashMap<>();
    //未初始化的bean，有依赖的bean
    private static final LinkedHashMap<String, Class<?>> beanCacheMap = new LinkedHashMap<>();

    public static void putBeanCacheMap(String name, Class<?> obj) {
        beanCacheMap.put(name, obj);
    }
    public static LinkedHashMap<String, Class<?>> getBeanCaches() {
        return beanCacheMap;
    }
    public static Class<?> getBeanCache(String name) {
        return beanCacheMap.get(name);
    }

    public static ConcurrentHashMap<String, Method> getMapping() {
        return mapping;
    }

    public static Method getMethod(String path) {
        return mapping.get(path);
    }

    public static void putMethod(String path, Method method) {
        mapping.put(path, method);
    }

    public static Object getBeanMap(String name) {
        return beanMap.get(name);
    }

    public static void getAll() {
        for (String s : beanMap.keySet()) {
            System.err.println(s);
        }
    }
    public static void putBeanMap(String name, Object obj) {
        beanMap.put(name, obj);
    }

    public static boolean containsKey(String name) {
        return beanMap.containsKey(name);
    }
    public static boolean containsMappingKey(String name) {
        return mapping.containsKey(name);
    }
}
