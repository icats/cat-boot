package cn.xhteam.boot.core.util;

public final class StringUtil {
    public static final String EMPTY = "";
    public static final String BLANK = " ";

    private StringUtil() {
    }

    public static boolean isUpperCase(String string) {
        if (isEmpty(string)) {
            return false;
        } else {
            char[] characters = string.toCharArray();
            char[] var2 = characters;
            int var3 = characters.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                char c = var2[var4];
                if (!Character.isUpperCase(c)) {
                    return false;
                }
            }

            return true;
        }
    }

    public static boolean isLowerCase(String string) {
        if (isEmpty(string)) {
            return false;
        } else {
            char[] characters = string.toCharArray();
            char[] var2 = characters;
            int var3 = characters.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                char c = var2[var4];
                if (!Character.isLowerCase(c)) {
                    return false;
                }
            }

            return true;
        }
    }

    public static boolean containsUppercase(String string) {
        if (isEmpty(string)) {
            return false;
        } else {
            char[] characters = string.toCharArray();
            char[] var2 = characters;
            int var3 = characters.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                char c = var2[var4];
                if (Character.isUpperCase(c)) {
                    return true;
                }
            }

            return false;
        }
    }

    public static boolean containsLowercase(String string) {
        if (isEmpty(string)) {
            return false;
        } else {
            char[] characters = string.toCharArray();
            char[] var2 = characters;
            int var3 = characters.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                char c = var2[var4];
                if (Character.isLowerCase(c)) {
                    return true;
                }
            }

            return false;
        }
    }

    public static boolean isEmpty(String string) {
        return null == string || "".equals(string);
    }

    public static boolean isNotEmpty(String string) {
        return !isEmpty(string);
    }

    public static boolean isBlank(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    public static String[] splitByAnyBlank(String string) {
        if (isEmpty(string)) {
            return new String[0];
        } else {
            String pattern = "\\s+";
            return string.split("\\s+");
        }
    }

    public static String getCamelCaseString(String inputString, boolean firstCharacterUppercase) {
        StringBuilder sb = new StringBuilder();
        boolean nextUpperCase = false;

        for (int i = 0; i < inputString.length(); ++i) {
            char c = inputString.charAt(i);
            switch (c) {
                case ' ':
                case '#':
                case '$':
                case '&':
                case '-':
                case '/':
                case '@':
                case '_':
                    if (sb.length() > 0) {
                        nextUpperCase = true;
                    }
                    break;
                default:
                    if (nextUpperCase) {
                        sb.append(Character.toUpperCase(c));
                        nextUpperCase = false;
                    } else {
                        sb.append(Character.toLowerCase(c));
                    }
            }
        }

        if (firstCharacterUppercase) {
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        }

        return sb.toString();
    }

    public static String firstToLowerCase(String str) {
        if (str != null && str.trim().length() != 0) {
            return str.length() == 1 ? str.toLowerCase() : str.substring(0, 1).toLowerCase() + str.substring(1);
        } else {
            return str;
        }
    }

    public static String firstToUpperCase(String str) {
        if (str != null && str.trim().length() != 0) {
            return str.length() == 1 ? str.toUpperCase() : str.substring(0, 1).toUpperCase() + str.substring(1);
        } else {
            return str;
        }
    }

    public static String defaultEmpty(String string) {
        return isEmpty(string) ? "" : string;
    }

    public static String join(Object[] array, String separator, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        } else {
            if (separator == null) {
                separator = "";
            }

            int noOfItems = endIndex - startIndex;
            if (noOfItems <= 0) {
                return "";
            } else {
                StringBuilder buf = new StringBuilder(noOfItems * 16);

                for (int i = startIndex; i <= endIndex; ++i) {
                    if (i > startIndex) {
                        buf.append(separator);
                    }

                    if (array[i] != null) {
                        buf.append(array[i]);
                    }
                }

                return buf.toString();
            }
        }
    }

    public static String camelToUnderline(String camelStr) {
        if (isEmpty(camelStr)) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            char[] chars = camelStr.toCharArray();
            char[] var3 = chars;
            int var4 = chars.length;

            for (int var5 = 0; var5 < var4; ++var5) {
                char c = var3[var5];
                if (Character.isUpperCase(c)) {
                    sb.append('_');
                    sb.append(Character.toLowerCase(c));
                } else {
                    sb.append(c);
                }
            }

            return sb.toString();
        }
    }

    public static String underlineToCamel(String underlineStr) {
        if (isEmpty(underlineStr)) {
            return "";
        } else {
            int len = underlineStr.length();
            StringBuilder sb = new StringBuilder(len);

            for (int i = 0; i < len; ++i) {
                char c = underlineStr.charAt(i);
                if (c == '_') {
                    ++i;
                    if (i < len) {
                        sb.append(Character.toUpperCase(underlineStr.charAt(i)));
                    }
                } else {
                    sb.append(c);
                }
            }

            return sb.toString();
        }
    }

    public static String repeat(String component, int times) {
        if (!isEmpty(component) && times > 0) {
            StringBuilder stringBuffer = new StringBuilder();

            for (int i = 0; i < times; ++i) {
                stringBuffer.append(component);
            }

            return stringBuffer.toString();
        } else {
            return "";
        }
    }

    public static String buildString(Object original, String middle, int prefixLength) {
        if (ObjectUtil.isNull(original)) {
            return null;
        } else {
            String string = original.toString();
            int stringLength = string.length();
            String prefix = "";
            String suffix = "";
            if (stringLength >= prefixLength) {
                prefix = string.substring(0, prefixLength);
            } else {
                prefix = string.substring(0, stringLength);
            }

            int suffixLength = stringLength - prefix.length() - middle.length();
            if (suffixLength > 0) {
                suffix = string.substring(stringLength - suffixLength);
            }

            return prefix + middle + suffix;
        }
    }

    public static String trim(String original) {
        return isBlank(original) ? original : original.trim();
    }

    public static String nullToDefault(CharSequence str, String defaultStr) {
        return str == null ? defaultStr : str.toString();
    }

    public static String fill(String str, char filledChar, int len, boolean isPre) {
        int strLen = str.length();
        if (strLen > len) {
            return str;
        } else {
            String filledStr = repeat(String.valueOf(filledChar), len - strLen);
            return isPre ? filledStr.concat(str) : str.concat(filledStr);
        }
    }

    @Deprecated
    public static String times(String single, int times) {
        if (isEmpty(single)) {
            return single;
        } else if (times <= 0) {
            return single;
        } else {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < times; ++i) {
                stringBuilder.append(single);
            }

            return stringBuilder.toString();
        }
    }

    public static String capitalFirst(String string) {
        if (isEmpty(string)) {
            return string;
        } else if (string.length() <= 1) {
            return string.toUpperCase();
        } else {
            char capitalChar = Character.toUpperCase(string.charAt(0));
            return capitalChar + string.substring(1);
        }
    }

    @Deprecated
    private static char getPreChar(char preChar, char currentChar) {
        return '\\' == preChar && '\\' == currentChar ? ' ' : currentChar;
    }

    public static byte[] stringToBytes(String string) {
        return ObjectUtil.isNull(string) ? null : string.getBytes();
    }

    public static String[] splitToStringArray(String string, String splitter) {
        return isEmpty(string) ? null : string.split(splitter);
    }

    public static String[] splitToStringArray(String string) {
        return splitToStringArray(string, ",");
    }

    //字符串添加前缀方法
    public static String addSlashMissing(String str) {
        if (isEmpty(str)) {
            return "";
        }
        if (!str.startsWith("/")) {
            str = "/" + str;
        }
        return str;
    }
}