package cn.xhteam.boot.core.enums;

/**
 * 请求方法枚举
 */
public enum HttpMethod {
    GET, POST, PUT, DELETE, PATCH
}
