package cn.xhteam.boot.core.util;

import java.io.*;
import java.nio.file.Files;
import java.util.List;

public class FileUtil {
    public static File inputStreamToFile(InputStream inputStream) {
        try {
            File tempFile = File.createTempFile("temp", ".tmp");
            OutputStream outputStream = Files.newOutputStream(tempFile.toPath());
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }
            inputStream.close();
            outputStream.close();
            File file = new File(tempFile.getAbsolutePath());
            System.out.println("File: " + file.getAbsolutePath());
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void findFiles(List<File> list, File directory,String suffix) {
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    findFiles(list, file,suffix);
                }
                if (file.isFile()) {
                    if(StringUtil.isNotBlank(suffix) && !file.getName().endsWith(suffix)){
                        continue;
                    }
                    list.add(file);
                }
            }
        }
    }
}
