package cn.xhteam.boot.core.util;

public class ObjectUtil {
    public static boolean isNull(Object obj) {
        return null == obj || obj.equals(null);
    }

    public static boolean isNotNull(Object obj) {
        return null != obj && false == obj.equals(null);
    }
}
