package cn.xhteam.boot.core.util;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class BeanUtil {
    public static String getClassBeanName(Class<?> classObj) {
        return getClassBeanName(classObj, null);
    }

    public static String getClassBeanName(Class<?> classObj, String beanName) {
        String className = classObj.getTypeName();
        return StringUtil.isNotBlank(beanName) ? className + "." + beanName : className;
    }

    public static String getMethodsBeanName(Method method) {
        return method.getClass().getTypeName() + "." + method.getName();
    }

    public static Class<?> getClassByName(String className) throws ClassNotFoundException {
        return Class.forName(className);
    }

    public static String getParameterBeanName(Parameter parameter) {
        return parameter.getType().getTypeName();
    }
}
