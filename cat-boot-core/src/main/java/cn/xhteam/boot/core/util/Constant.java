package cn.xhteam.boot.core.util;

import com.alibaba.fastjson2.util.DateUtils;

import java.util.Date;

public class Constant {
    public static final String CHARSET_UTF_8 = ";charset=utf-8";
    public static final String CONTENT_APPLICATION_JSON = "application/json";
    public static final String CONTENT_APPLICATION_FORM_URLENCODED= "application/x-www-form-urlencoded";
    public static final String CONTENT_APPLICATION_FORM_DATA= "application/form-data";
    public static final String CONTENT_MULTIPART_FORM_DATA= "multipart/form-data";
    public static final String CONTENT_TEXT_HTML = "text/html";

    public static final String RESPONSE_404_HTML_EN = "<html><body><h1>Whitelabel Error Page</h1><p>This application has no explicit mapping for /error, so you are seeing this as a fallback.</p><div id='created'>" + new Date() + "</div><div>There was an unexpected error (type=Not Found, status=404).</div><div>No message available</div></body></html>";
    public static final String RESPONSE_404_HTML_ZN = "<html><body><h1>地址错误！！</h1><p>此应用程序没有/error的显式映射，因此您将其视为回退。</p><div id='created'>" + DateUtils.format(new Date()) + "</div><div>出现意外错误（类型=未找到，状态=404）。</div><div>没有可用的消息</div></body></html>";
    public static final String HTTP_REASON = "Not Found";

    public static final String resourceProperties = "/application.properties";
    public static final String resourceYml = "/application.yml";

    public static final String catBootProperties = "/cat-boot.properties";
    public static final String banner = "/banner";
    public static final String txtSuffix = ".txt";
    public static final String defaultBanner = "-default";
    public static final String textLine = "-";

}
