package cn.xhteam.boot.core.properties;

import cn.xhteam.boot.core.util.Constant;
import cn.xhteam.boot.core.util.FileClassUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class PropertiesConfig extends Constant {
    public static final Logger logger = LoggerFactory.getLogger(PropertiesConfig.class);
    private static Properties props; //配置

    private static HashMap<String, InputStream> resourceMap;

    private static HashMap<String, InputStream> getResource(Class BootClass) {
        resourceMap = new HashMap<>();
        InputStream inputStream = BootClass.getResourceAsStream(resourceProperties);
        String key = resourceProperties;
        if (null == inputStream) {
            key = resourceYml;
            inputStream = BootClass.getResourceAsStream(resourceYml);
            if (null != inputStream) {
                resourceMap.put(key, inputStream);
            }
            return resourceMap;
        }
        resourceMap.put(key, inputStream);
        return resourceMap;
    }

    public static void loadConfig(Class BootClass) {
        try {
            props = new Properties();
            HashMap<String, InputStream> map = getResource(BootClass);
            if (!map.isEmpty()) {
                if (map.containsKey(resourceProperties)) {
                    BufferedInputStream bis = new BufferedInputStream(map.get(resourceProperties));
                    props.load(bis);
                } else if (map.containsKey(resourceYml)) {
                    Yaml yaml = new Yaml();
                    Object object = yaml.load(map.get(resourceYml));
                    List<String> resultList = FileClassUtil.travelRootWithResult(object);
                    for (String str : resultList) {
                        String[] arr = str.split("=", 2);
                        if (arr.length == 2) {
                            props.setProperty(arr[0], arr[1]);
                        }
                    }
                }
            }
            setLocalProperties(BootClass);
        } catch (IOException e) {
            logger.error("读取配置文件出错" + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private static void setLocalProperties(Class BootClass) {
        try {
            InputStream inputStream = BootClass.getResourceAsStream(catBootProperties);
            if (null != inputStream) {
                BufferedInputStream bis = new BufferedInputStream(inputStream);
                Properties properties = new Properties();
                properties.load(bis);
                for (String key : properties.stringPropertyNames()) {
                    String value = properties.getProperty(key);
                    props.setProperty(key, value);
                }
            }
        } catch (IOException e) {
            logger.error("读取配置文件出错" + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public static String getProperty(String key, String defaultValue) {
        if (null != props) {
            return props.getProperty(key, defaultValue);
        }
        return defaultValue;
    }

    public static Properties getProperties() {
        return props;
    }

    public static String getProperty(String key) {
        return getProperty(key, null);
    }

}
