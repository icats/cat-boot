package cn.xhteam.boot.mybatis.loader;

import cn.xhteam.boot.core.bean.HandContainer;
import cn.xhteam.boot.core.properties.PropertiesConfig;
import cn.xhteam.boot.core.stereotype.Bean;
import cn.xhteam.boot.core.stereotype.Initialized;
import cn.xhteam.boot.core.util.BeanUtil;
import cn.xhteam.boot.core.util.FileClassUtil;
import cn.xhteam.boot.core.util.ObjectUtil;
import cn.xhteam.boot.core.util.StringUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.defaults.DefaultSqlSessionFactory;

import java.util.List;

@Initialized
public class MapperAutoConfiguration {
    @Bean
    public void initializedMapperBean() {
        String packageName = PropertiesConfig.getProperty("mybatis-plus.mapper-package");
        if (StringUtil.isEmpty(packageName)) {
            throw new RuntimeException("mybatis-plus.mapper-package 需要指定mapper包名...");
        }
        List<Class<?>> list = FileClassUtil.getClasses(packageName);
        if (list.isEmpty()) {
            throw new RuntimeException(packageName + "目录下扫描到0个mapper...");
        }
        String beanName = BeanUtil.getClassBeanName(DefaultSqlSessionFactory.class);
        Object obj = HandContainer.getBeanMap(beanName);
        if (ObjectUtil.isNull(obj)) {
            throw new RuntimeException("SqlSession 未初始化容器，检查config配置是否加载..." + beanName);
        }
        DefaultSqlSessionFactory sessionFactory = (DefaultSqlSessionFactory) obj;
        SqlSession sqlSession = sessionFactory.openSession(true);
        for (Class<?> cls : list) {
            HandContainer.putBeanMap(BeanUtil.getClassBeanName(cls), sqlSession.getMapper(cls));
        }
    }
}
