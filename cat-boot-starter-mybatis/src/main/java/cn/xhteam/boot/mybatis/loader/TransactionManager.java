package cn.xhteam.boot.mybatis.loader;

import cn.xhteam.boot.core.bean.HandContainer;
import cn.xhteam.boot.core.util.BeanUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.defaults.DefaultSqlSession;
import org.apache.ibatis.session.defaults.DefaultSqlSessionFactory;

public class TransactionManager {
    private static final ThreadLocal<SqlSession> sessionThreadLocal = new ThreadLocal<>();

    // 开始事务，获取会话
    public static void beginTransaction(Class<?> cls) {

        String beanName = BeanUtil.getClassBeanName(cls);

        DefaultSqlSession sqlSession  = (DefaultSqlSession) HandContainer.getBeanMap(beanName);
        sessionThreadLocal.set(sqlSession);
    }

    // 提交事务
    public static void commit() {
        SqlSession session = sessionThreadLocal.get();
        if (session != null) {
            session.commit();
        }
    }

    // 回滚事务
    public static void rollback() {
        SqlSession session = sessionThreadLocal.get();
        if (session != null) {
            session.rollback();
        }
    }

    // 关闭事务
    public static void close() {
        SqlSession session = sessionThreadLocal.get();
        if (session != null) {
            session.close();
        }
        sessionThreadLocal.remove();
    }

    // 获取当前会话
    public static SqlSession getCurrentSession() {
        return sessionThreadLocal.get();
    }
}
