package cn.xhteam.boot.mybatis.loader;

import cn.xhteam.boot.core.stereotype.Transactional;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class TransactionProxy implements InvocationHandler {

    private Object target;
    private SqlSessionFactory sqlSessionFactory;

    public TransactionProxy(Object target, SqlSessionFactory sqlSessionFactory) {
        this.target = target;
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // 检查方法是否有 @Transactional 注解
        if (method.isAnnotationPresent(Transactional.class)) {
            try (SqlSession sqlSession = sqlSessionFactory.openSession(false)) {
                Object result = null;
                try {
                    result = method.invoke(target, args);  // 执行方法
                    sqlSession.commit();  // 提交事务
                } catch (Exception e) {
                    sqlSession.rollback();  // 出现异常时回滚事务
                    throw e;
                }
                return result;
            }
        } else {
            // 如果没有 @Transactional 注解，直接执行方法
            return method.invoke(target, args);
        }
    }

    // 创建代理对象
    public static Object createProxy(Object target, SqlSessionFactory sqlSessionFactory) {
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                new TransactionProxy(target, sqlSessionFactory)
        );
    }
}
