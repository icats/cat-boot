package cn.xhteam.boot.mybatis.configuration;

import cn.xhteam.boot.core.properties.PropertiesConfig;
import cn.xhteam.boot.core.stereotype.Bean;
import cn.xhteam.boot.core.stereotype.Configuration;
import cn.xhteam.boot.core.util.FileUtil;
import cn.xhteam.boot.core.util.StringUtil;
import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.ClassUtils;
import com.baomidou.mybatisplus.core.toolkit.GlobalConfigUtils;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.jdk14.Jdk14LoggingImpl;
import org.apache.ibatis.logging.log4j.Log4jImpl;
import org.apache.ibatis.logging.log4j2.Log4j2Impl;
import org.apache.ibatis.logging.nologging.NoLoggingImpl;
import org.apache.ibatis.logging.slf4j.Slf4jImpl;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@Configuration
public class MyBatisConfig {

    public DataSource getDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(PropertiesConfig.getProperty("catBoot.datasource.driverClassName"));
        dataSource.setUrl(PropertiesConfig.getProperty("catBoot.datasource.druid.url"));
        dataSource.setUsername(PropertiesConfig.getProperty("catBoot.datasource.druid.username"));
        dataSource.setPassword(PropertiesConfig.getProperty("catBoot.datasource.druid.password"));
        dataSource.setMaxWait(Integer.parseInt(PropertiesConfig.getProperty("catBoot.datasource.druid.maxWait", "10000")));
        return dataSource;
    }

    @Bean
    public SqlSessionFactory getSessionFactory() throws IOException, SQLException {
        String packageName = PropertiesConfig.getProperty("mybatis-plus.mapper-package");
        if (StringUtil.isEmpty(packageName)) {
            throw new RuntimeException("mybatis-plus.mapper-package 需要指定mapper包名...");
        }
        String mapperLocations = PropertiesConfig.getProperty("mybatis-plus.mapper-locations", "mapper");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        //这是mybatis-plus的配置对象，对mybatis的Configuration进行增强
        MybatisConfiguration configuration = new MybatisConfiguration();
        //这是初始化配置，后面会添加这部分代码
        initConfiguration(configuration);
        //这是初始化连接器，如mybatis-plus的分页插件
        configuration.addInterceptor(initInterceptor());
        configuration.setUseActualParamName(true);
        //配置日志实现
        String packageLog = PropertiesConfig.getProperty("mybatis-plus.log", "org.apache.ibatis.logging.stdout.StdOutImpl");
        configuration.setLogImpl((Class<? extends Log>) ClassUtils.toClassConfident(packageLog));
        //扫描mapper接口所在包
        configuration.addMappers(packageName);
        //构建mybatis-plus需要的globalconfig
        GlobalConfig globalConfig = new GlobalConfig();
        //此参数会自动生成实现baseMapper的基础方法映射
//        globalConfig.setSqlInjector(new DefaultSqlInjector());
        //设置id生成器
        globalConfig.setIdentifierGenerator(new DefaultIdentifierGenerator());
        //设置超类mapper
        globalConfig.setSuperMapperClass(BaseMapper.class);
        //给configuration注入GlobalConfig里面的配置
        GlobalConfigUtils.setGlobalConfig(configuration, globalConfig);
        //设置数据源
        Environment environment = new Environment("1", new JdbcTransactionFactory(), getDataSource());
        configuration.setEnvironment(environment);
        this.loadMappers(configuration, mapperLocations);
        //构建sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = builder.build(configuration);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        sqlSession.getConnection().isValid(Integer.parseInt(PropertiesConfig.getProperty("catBoot.datasource.druid.connectTimeout", "100000")));
        sqlSession.close();
        //创建session
        return sqlSessionFactory;
    }

    private void initConfiguration(MybatisConfiguration configuration) {
        //开启驼峰大小写转换
        configuration.setMapUnderscoreToCamelCase(true);
        //配置添加数据自动返回数据主键
        configuration.setUseGeneratedKeys(true);
    }

    private Interceptor initInterceptor() {
        //创建mybatis-plus插件对象
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //构建分页插件
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setDbType(DbType.MYSQL);
        paginationInnerInterceptor.setOverflow(true);
        paginationInnerInterceptor.setMaxLimit(500L);
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        return interceptor;
    }

    public void loadMappers(MybatisConfiguration configuration, String mapperLocationPattern) throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Enumeration<URL> resources = classLoader.getResources(mapperLocationPattern);

        while (resources.hasMoreElements()) {
            URL resourceUrl = resources.nextElement();
            File resourceFile = new File(resourceUrl.getFile());

            if (resourceFile.exists()) {
                if (resourceFile.isDirectory()) {
                    List<File> mapperFiles = findMapperFiles(resourceFile);
                    processMapperFiles(mapperFiles, configuration);
                } else {
                    processSingleMapperFile(resourceFile, configuration);
                }
            }
        }
    }

    private List<File> findMapperFiles(File directory) {
        List<File> list = new ArrayList<>();
        FileUtil.findFiles(list, directory, ".xml");
        return list;
    }

    private void processMapperFiles(List<File> mapperFiles, MybatisConfiguration configuration) throws IOException {
        for (File mapperFile : mapperFiles) {
            try (InputStream inputStream = Files.newInputStream(mapperFile.toPath())) {
                XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(inputStream, configuration, mapperFile.toString(), configuration.getSqlFragments());
                xmlMapperBuilder.parse();
            }
        }
    }

    private void processSingleMapperFile(File mapperFile, MybatisConfiguration configuration) throws IOException {
        try (InputStream inputStream = Files.newInputStream(mapperFile.toPath())) {
            XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(inputStream, configuration, mapperFile.toString(), configuration.getSqlFragments());
            xmlMapperBuilder.parse();
        }
    }
}
